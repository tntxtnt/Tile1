﻿using UnityEngine;
using System.Collections;

public class Moveable : MonoBehaviour
{
    // moving
    [Range(4f,10f)]
    public float moveForce = 2f;
    public float maxHorizontalSpeed = 2.5f;
    public float maxFallSpeed = 5f;
    public float airMoveModifier = 0.2f; //moving horizontally in air should be reduced
    private bool facingRight = true;
    private bool previouslyCantMove = false; //to make jumping into 3-hole space easier
    private bool[] clearFronts = new bool[4]; //frontCheck
    private float groundRaycastDist = 0.04f; //groundCheck
    private float frontRaycastDist = 0.02f; //frontCheck
    [HideInInspector]
    public float h = 0f; //set moving direction, for controlling purpose just make it public

    // jumping
    public float jumpForce = 130f;
    public int maxJumpCount = 2;
    public float midAirJumpModifier = 0.9f; //jumping mid air should be reduced
    private int jumpRemainings = 0;

    public Transform groundCheck1;
    public Transform groundCheck2;
    public Transform groundCheck3;
    public Transform groundCheck4;
    public LayerMask whatIsTerrain;
    public LayerMask whatIsGround;
    public LayerMask whatIsSteep;
    public Transform frontCheck1;
    public Transform frontCheck2;
    public Transform frontCheck3;
    public Transform frontCheck4;

    private Rigidbody2D rb2d;
    private Animator anim;
    private WeaponManager weaponManager; //if the activeWeapon is currently in use,
                                         //then face with the same direction of the weapon

    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        weaponManager = GetComponent<WeaponManager>();
    }

    void FixedUpdate() //maximum of 4+3+3 raycast. Normally 4+1+2 raycast, where the last raycasts are very light
    {
        Vector2 raycastDir = facingRight ? Vector2.right : Vector2.left;

        // Check grounded
        bool groundedNormal = CheckGrounded();
        bool groundedSteep = Physics2D.Raycast(groundCheck1.position, raycastDir, 0.48f, whatIsSteep);
        bool isGrounded = groundedNormal || groundedSteep;
        // fall => -1 jump remainings
        if (anim.GetBool("Grounded") && !isGrounded)
            jumpRemainings--;
        // set grounded bool
        anim.SetBool("Grounded", isGrounded);
        // refill jump count
        if (isGrounded) //magic number
            jumpRemainings = maxJumpCount;

        // Check fronts
        CheckFrontAll();

        // Handle horizontal movement
        anim.SetBool("Moving", Mathf.Abs(h) > Mathf.Epsilon);
        Vector2 mf = Vector2.right * h * moveForce;
        if (!isGrounded)
            mf *= airMoveModifier;
        // Flip?
        bool flip = (h < 0 && facingRight) || (h > 0 && !facingRight);
        bool attacking = weaponManager != null && weaponManager.activeWeapon != null && weaponManager.activeWeapon.IsCooldown();
        if (attacking)
            flip = false;
        if (flip)
            Flip();
        if (Mathf.Abs(rb2d.velocity.x) < maxHorizontalSpeed)
            rb2d.AddForce(mf);
        if (Mathf.Abs(rb2d.velocity.x) > maxHorizontalSpeed)
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxHorizontalSpeed, rb2d.velocity.y);

        // Max fall speed
        if (Mathf.Abs(rb2d.velocity.y) > maxFallSpeed)
            rb2d.velocity = new Vector2(rb2d.velocity.x, Mathf.Sign(rb2d.velocity.y) * maxFallSpeed);

        // Handle stairs
        if (Mathf.Abs(h) > 0.3f)
            if (HasStairInFront() && isGrounded)
            {
                //Debug.Log("hasStairInFront");
                float addX = 0.06f;
                float addY = 0.24f;
                transform.position = new Vector2(transform.position.x + Mathf.Sign(h) * addX, transform.position.y + addY);
                rb2d.velocity = new Vector2(Mathf.Max(rb2d.velocity.x, maxHorizontalSpeed / 2f * h), rb2d.velocity.y);
            }

        // cant move?
        bool cantMove = !clearFronts[1] || !clearFronts[2];
        if (!isGrounded && !clearFronts[0])
            cantMove = true;
        // jump into 3-tile hole/door/entrance
        if (!cantMove && previouslyCantMove && !isGrounded) //previously can't move but now can && in mid air
            if (Mathf.Abs(h) > Mathf.Epsilon)
                transform.position = new Vector2(transform.position.x + h * 0.04f, transform.position.y);
        previouslyCantMove = cantMove;


        bool facingSteep = Physics2D.Raycast(groundCheck2.position, raycastDir, 0.06f, whatIsSteep); //magic number 0.06
        bool faceoutSteep = Physics2D.Raycast(groundCheck4.position, -raycastDir, 0.06f, whatIsSteep);
        if (attacking && Mathf.Sign(weaponManager.activeWeapon.GetAttackDirection().x) != Mathf.Sign(h))
        {
            bool temp = facingSteep;
            facingSteep = faceoutSteep;
            faceoutSteep = temp;
        }
        // on steep?
        if ((!groundedNormal && groundedSteep) || (isGrounded && facingSteep))
        {
            if (Mathf.Abs(h) > Mathf.Epsilon)
            {
                float maxAscendSpeed = maxHorizontalSpeed * .95f;
                float addedY = moveForce * 1.25f;
                if (facingSteep)
                {
                    if (groundedNormal)
                    {
                        float addedVelocity = h * rb2d.velocity.x / maxHorizontalSpeed;
                        rb2d.velocity = new Vector2(rb2d.velocity.x, rb2d.velocity.x);
                    }
                    if (rb2d.velocity.y <= maxAscendSpeed)
                    {
                        rb2d.AddForce(Vector2.up * addedY * Mathf.Abs(h));
                        if (rb2d.velocity.y > maxAscendSpeed)
                            rb2d.velocity = new Vector2(rb2d.velocity.x, maxAscendSpeed);
                    }
                    if (rb2d.velocity.y > maxHorizontalSpeed)
                        rb2d.velocity = new Vector2(rb2d.velocity.x, maxHorizontalSpeed);
                }
                else
                {
                    // one small bug out of the top of the steep
                    if (Physics2D.Raycast(groundCheck2.position, Vector2.down, 0.04f, whatIsSteep))
                    {
                        if (rb2d.velocity.y <= maxAscendSpeed)
                            rb2d.AddForce(Vector2.up * addedY * Mathf.Abs(h));
                    }
                    // descending
                    else if (faceoutSteep)
                    {
                        if (Mathf.Abs(rb2d.velocity.x) > Mathf.Abs(rb2d.velocity.y))
                            rb2d.velocity = new Vector2(rb2d.velocity.y * .95f, rb2d.velocity.y);
                    }
                    // moving on top of the steep
                    else
                    {
                        if (Mathf.Abs(rb2d.velocity.x) < maxHorizontalSpeed)
                            rb2d.AddForce(Vector2.right * h * 3f); //magic number 3f to overcome 4f friction
                        if (Mathf.Abs(rb2d.velocity.x) > maxHorizontalSpeed)
                            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxHorizontalSpeed, rb2d.velocity.y);
                    }
                }
            }
        }

    }


    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theFace = transform.localScale;
        theFace.x *= -1;
        transform.localScale = theFace;
    }

    bool CheckGrounded()
    {
        return Physics2D.Raycast(groundCheck3.position, Vector2.down, groundRaycastDist, whatIsGround)
            || Physics2D.Raycast(groundCheck2.position, Vector2.down, groundRaycastDist, whatIsGround)
            || Physics2D.Raycast(groundCheck4.position, Vector2.down, groundRaycastDist, whatIsGround);
    }

    void CheckFrontAll()
    {
        Vector2 dir = facingRight ? Vector2.right : Vector2.left;
        clearFronts[0] = !Physics2D.Raycast(frontCheck1.position, dir, frontRaycastDist, whatIsTerrain);
        clearFronts[1] = !Physics2D.Raycast(frontCheck2.position, dir, frontRaycastDist, whatIsTerrain);
        clearFronts[2] = !Physics2D.Raycast(frontCheck3.position, dir, frontRaycastDist, whatIsTerrain);
        clearFronts[3] = !Physics2D.Raycast(frontCheck4.position, dir, frontRaycastDist, whatIsTerrain);
    }

    bool HasStairInFront()
    {
        if (clearFronts[0] || !clearFronts[3]) // can't go up 1 tile if there's another tile on top of that stair
            return false;
        return clearFronts[1] && clearFronts[2];
    }

    public void Jump()
    {
        if (jumpRemainings > 0)
        {
            rb2d.velocity = new Vector2(rb2d.velocity.x, 0); //refresh y velocity
            Vector2 jf = Vector2.up * jumpForce;
            if (!anim.GetBool("Grounded")) //first jumpRemainings from ground is managed by FixedUpdate
            {
                --jumpRemainings;
                jf *= midAirJumpModifier;
            }
            rb2d.AddForce(jf);
        }
    }
}
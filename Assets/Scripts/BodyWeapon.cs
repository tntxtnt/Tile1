﻿using UnityEngine;
using System.Collections;

public class BodyWeapon : Weapon
{
    public float damage = 0f;
    public float pushForce = 0f;
    public Vector2 pushDirection = new Vector2(1f, .5f);

    void Awake()
    {
        pushDirection = pushDirection.normalized * pushForce;
        Activate();
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag(enemyTag))
        {
            bool forceRight = (other.transform.position - transform.position).x > 0;
            if (forceRight && pushDirection.x < 0f)
                pushDirection = new Vector2(pushDirection.x * -1, pushDirection.y);
            else if (!forceRight && pushDirection.x > 0f)
                pushDirection = new Vector2(pushDirection.x * -1, pushDirection.y);
            other.BroadcastMessage("ApplyDamage", new Damage(damage, pushDirection));
        }
    }

    public override void SwitchAmmo() { }

    protected override void Attack() { }

    public override bool IsCooldown() { return false; } //always active
}

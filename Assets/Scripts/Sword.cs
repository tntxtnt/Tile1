﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sword : Weapon
{
    public float swingTime = 1f;
    public float baseDamage = 10f;
    public float knockbackForce = 100f;
    public float swingArcBeginAngle = 135f;
    public float swingArcEndAngle = -30f;
    public Vector2 knockbackDirection = new Vector2(1, 0);
    public float knockbackReduction = 0.33f;
    public float baseKnockbackReduction = 0.1f;

    //private bool swing = false;
    private int swingIndex = -1;
    private int totalSwingIntervals;
    private float swingAnglePerInterval;

    // list of damaged enemies (to prevent further damages to them)
    private HashSet<Transform> dealtDamageObjects = new HashSet<Transform>();

    private BoxCollider2D boxCollider;
    private SpriteRenderer spriteRenderer;

    public LayerMask hitLayers;
    public Transform lineCheck;

	void Awake()
    {
        totalSwingIntervals = (int) (swingTime / Time.fixedDeltaTime);
        swingAnglePerInterval = (swingArcBeginAngle - swingArcEndAngle) / totalSwingIntervals;

        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;

        boxCollider = GetComponent<BoxCollider2D>();
        boxCollider.enabled = false;
	}

    void Start()
    {
        lineCheck = transform.parent.FindChild("FrontCheck2");
    }

    public override void SwitchAmmo()
    {
        // do nothing!
    }

    public override bool IsCooldown()
    {
        return spriteRenderer.enabled;
    }

    protected override void Attack()
    {
        if (IsCooldown()) return;

        StartCoroutine(Swing());
    }

    IEnumerator Swing()
    {
        spriteRenderer.enabled = true;
        boxCollider.enabled = true;
        dealtDamageObjects.Clear();

        // rotate sword
        float angle = swingAnglePerInterval * swingIndex;
        transform.rotation = Quaternion.Euler(0, 0, swingArcBeginAngle);
        for (int i = totalSwingIntervals; i > 0; --i)
        {
            transform.Rotate(Vector3.forward, angle);
            yield return new WaitForSeconds(Time.fixedDeltaTime);
        }

        spriteRenderer.enabled = false;
        boxCollider.enabled = false;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Transform badGuy = null;
        if (other.CompareTag(enemyTag))
            badGuy = other.transform;

        if (badGuy == null)
            return;
        if (dealtDamageObjects.Contains(badGuy)) // already hit this guy
            return;

        // Prevent hitting through wall
        RaycastHit2D hit = Physics2D.Linecast(lineCheck.position, badGuy.transform.position, hitLayers);
        if (hit) return;

        // knock back reduction, based on hit counts (not on distance from sword)
        float kbReduction = 1f - knockbackReduction * dealtDamageObjects.Count;
        if (kbReduction < baseKnockbackReduction)
            kbReduction = baseKnockbackReduction;
        Vector2 force = knockbackDirection.normalized * kbReduction * knockbackForce;
        if (transform.parent.localScale.x < 0)
            force.x *= -1;

        // add this guy to already damaged list to help him not getting hit again in one swing
        dealtDamageObjects.Add(badGuy);

        // apply damage
        badGuy.BroadcastMessage("ApplyDamage", new Damage(baseDamage, force));
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponManager : MonoBehaviour
{
    [SerializeField]
    private List<Weapon> weaponList;

    [HideInInspector]
    public Weapon activeWeapon = null; // only 1 weapon at a time

    public string enemyTag;

	void Awake()
    {
        SetWeapon(0);
	}
	
	void Update()
    {
        int id = -1;
        if (Input.GetKeyDown(KeyCode.Alpha1)) id = 0;
        if (Input.GetKeyDown(KeyCode.Alpha2)) id = 1;
        if (Input.GetKeyDown(KeyCode.Alpha3)) id = 2;

        if (id != -1)
            SetWeapon(id);
	}

    void SetWeapon(int id)
    {
        // destroy current weapon if it's not the same (no item variety so check same name is enough)
        if (activeWeapon && activeWeapon.name != weaponList[id].name)
        {
            Destroy(activeWeapon.gameObject);
            activeWeapon = null;
        }
        if (activeWeapon)
            return;
        activeWeapon = Instantiate(weaponList[id], transform.position, Quaternion.identity) as Weapon;
        activeWeapon.transform.parent = transform;
        activeWeapon.enemyTag = enemyTag;
        activeWeapon.transform.localScale = Vector3.one;
        activeWeapon.transform.localPosition = weaponList[id].transform.position;
        activeWeapon.transform.localRotation = weaponList[id].transform.rotation;
        activeWeapon.name = weaponList[id].name;
    }
}

﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour
{
    public float baseDamage = 2f;
    public float knockbackForce = 1f;
    public Vector2 knockbackDirection = new Vector2(1, 0);

    public AudioClip hitSound;

    private string enemyTag;

    private Rigidbody2D rb2d;

    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        knockbackDirection = knockbackDirection.normalized;
    }

    void FixedUpdate()
    {
        float angle = Vector2.Angle(Vector2.right, rb2d.velocity);
        if (rb2d.velocity.y < 0)
            angle = -angle;
        transform.rotation = Quaternion.Euler(0, 0, angle);
    }

    public void AddBowDamage(float amount)
    {
        baseDamage += amount;
    }

    public void ApplyForce(Vector2 force)
    {
        rb2d.AddForce(force);
    }

    public void SetEnemyTag(string e)
    {
        enemyTag = e;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(enemyTag))
        {
            knockbackDirection = knockbackDirection * knockbackForce;
            if (rb2d.velocity.x < 0) knockbackDirection.x *= -1f;
            other.BroadcastMessage("ApplyDamage", new Damage(baseDamage, knockbackDirection));
            SoundManager.instance.PlaySingle(hitSound);
            Destroy(gameObject);
        }
        else if (other.CompareTag("Ground"))
            Destroy(gameObject);
    }
}

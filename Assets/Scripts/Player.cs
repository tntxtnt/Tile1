﻿using UnityEngine;
using System.Collections;

public class Player : Character
{
    public float hp = 20f;
    public float defense = 5f;
    public float damagePeriod = 0.5f;

    public AudioClip[] painSounds;

    private bool alive = true;
    private float damageCooldown = 0;
    private bool jump = false;

	private Rigidbody2D rb2d;
	private Animator anim;
    private Moveable movement;
    private SpriteRenderer spriteRenderer;
    private WeaponManager weaponManager;

	void Awake()
	{
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        movement = GetComponent<Moveable>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        weaponManager = GetComponent<WeaponManager>();
	}

    void Update()
    {
        if (!jump)
            jump = Input.GetButtonDown("Jump");

        // handle attack
        if (weaponManager.activeWeapon.IsInactive())
            if (Input.GetButtonDown("Fire1"))
            {
                weaponManager.activeWeapon.Activate();
                weaponManager.activeWeapon.SetAttackDirection(GetMouseDirection());
            }
        // stop attack
        if (Input.GetButtonUp("Fire1"))
            weaponManager.activeWeapon.Deactivate();

        // switch ammo
        if (Input.GetKeyDown(KeyCode.Q))
            weaponManager.activeWeapon.SwitchAmmo();
    }
	
	void FixedUpdate()
	{
        if (!alive) return;

        // handle invincible flashing after taking damage
        if (damageCooldown > 0)
        {
            damageCooldown -= Time.fixedDeltaTime;
            anim.SetFloat("DamageCooldown", damageCooldown);
        }
        else if (!GetComponent<Renderer>().enabled)
            spriteRenderer.enabled = true;

        // handle movement
        movement.h = Input.GetAxis("Horizontal");

        // handle jumping
        if (jump)
        {
            movement.Jump();
            jump = false;
        }

        // change weapon direction
        if (!weaponManager.activeWeapon.IsInactive())
            weaponManager.activeWeapon.SetAttackDirection(GetMouseDirection());
	}

    void Die()
    {
        alive = false;
        rb2d.velocity = new Vector3(0f, 0f, 0f);
        rb2d.isKinematic = true;
        Debug.Log("You die!");
    }

    public override void ApplyDamage(Damage d)
    {
        if (damageCooldown < 0.01f)
        {
            float dmg = d.damage - defense;
            if (dmg < 1f) dmg = 1f;
            hp -= dmg;
            rb2d.AddForce(d.force);
            SoundManager.instance.PlaySingle(painSounds[Random.Range(0, painSounds.Length)]);
            Debug.Log("Player Hp: " + hp);
            damageCooldown = damagePeriod;
            anim.SetFloat("DamageCooldown", damageCooldown);
            StartCoroutine(FlashSprites(damageCooldown, 0.1f));
            if (hp <= 0f)
                Die();
        }
    }

    IEnumerator FlashSprites(float duration, float delay)
    {
        int cycles = (int) (duration / delay);
        for (int i = 0; i < cycles; ++i)
        {
            spriteRenderer.enabled = !spriteRenderer.enabled;
            yield return new WaitForSeconds(delay);
        }
    }



    // Mouse manager
    Vector2 GetMousePosition()
    {
        return Camera.main.ScreenPointToRay(Input.mousePosition).origin;
    }

    Vector2 GetMouseDirection()
    {
        return GetMousePosition()  - (Vector2)transform.position;
    }
}

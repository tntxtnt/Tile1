﻿using UnityEngine;
using System.Collections;

public abstract class Weapon : MonoBehaviour
{
    public bool autoAttack = false;
    public string enemyTag;

    protected bool active = false;
    protected Vector2 attackDirection = Vector2.one;

    protected void FixedUpdate()
    {
        if (active)
        {
            if (!autoAttack)
                Deactivate();

            // Flip if character is facing the wrong way
            if (Mathf.Sign(transform.parent.localScale.x) != Mathf.Sign(attackDirection.x))
                transform.parent.BroadcastMessage("Flip");

            Attack();
        }
    }

    public bool IsInactive()
    {
        return !active && !IsCooldown();
    }

    public void Activate()
    {
        active = true;
    }

    public void Deactivate()
    {
        active = false;
    }

    public void SetAttackDirection(Vector2 dir)
    {
        attackDirection = dir;
    }

    public Vector2 GetAttackDirection()
    {
        return attackDirection;
    }

    public abstract void SwitchAmmo();

    protected abstract void Attack();

    public abstract bool IsCooldown();
}

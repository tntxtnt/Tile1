﻿using UnityEngine;
using System.Collections;
using System;

public class MapGenerator : MonoBehaviour
{
	public int width  = 100;
	public int height = 40;

    public string seed;

    [Range(0, 100)]
    public int fillPercentage;


    private int[,] map;

	[SerializeField]
	private GameObject[] groundTiles;
	private const float tileSize = 0.24f;

	// Use this for initialization
	void Start()
	{
        GenerateBase();

        for (int h = 1; h < 20; ++h) CreateBar(h, 18, 1);
        for (int h = 23; h < 55; ++h) CreateBar(h, 18, 1);
        for (int h = 1; h < 55; ++h) CreateBar(h, 21, 1);

        CreateBar(1, 0, 3);
        CreateBar(2, 0, 2);
        CreateBar(3, 0, 1);
        CreateBar(7, 0, 1);
        CreateBar(6, -5, 1);

        for (int i = 0; i < 30; ++i)
            CreateTile(-18+i, 1+i, groundTiles[2]);
        //CreateTile(12, 30, groundTiles[1]);
        for (int i = 0; i < 11; ++i)
            CreateTile(i, 10, groundTiles[2]);

        // stairs
        /*for (int i = 1; i < 15; ++i)
            CreateTile(-20 + i, i, GetRandomObject(groundTiles));
        for (int i = 15; i > 0; --i)
            CreateTile(40 - i, i, GetRandomObject(groundTiles));*/

		//CreateBar (3, 4, 10);
		//CreateBar (4, -9, 5);
		//CreateBar (7, -18, 5);


        //GenerateMap();
	}

    #region CellularAutomata;
    void GenerateMap()
    {
        map = new int[height, width];

        RandomFillMap();

        for (int i = 0; i < 5; ++i) SmoothMap();
        
        
        DrawMap();


    }

    void RandomFillMap()
    {
        if (seed.Length == 0)
            seed = Time.time.ToString();

        System.Random prng = new System.Random(seed.GetHashCode());

        for (int h = 0; h < height; ++h)
        {
            for (int w = 0; w < width; ++w)
            {
                if (h == 0 || h == height-1 || w == 0 || w == width-1)
                    map[h, w] = 1;
                else
                    map[h, w] = prng.Next(0,100) < fillPercentage ? 1 : 0;
            }
        }

    }

    void SmoothMap()
    {
        int[,] newMap = new int[height, width];
        for (int h = 0; h < height; ++h)
        {
            for (int w = 0; w < width; ++w)
            {
                int nbCount = GetNeightborCount(w, h);
                if (nbCount > 4)
                    newMap[h, w] = 1;
                else if (nbCount < 4)
                    newMap[h, w] = 0;
                else
                    newMap[h, w] = map[h, w];
            }
        }
        map = newMap;
    }

    int GetNeightborCount(int w, int h)
    {
        int count = 0;
        for (int x = w - 1; x <= w + 1; ++x)
        {
            for (int y = h - 1; y <= h + 1; ++y)
            {
                if (x >= 0 && x < width && y >= 0 && y < height)
                {
                    if (x != w || y != h)
                        count += map[y, x];
                }
                else
                {
                    ++count;
                }
            }
        }
        //if (w == 0 || w == width-1 || h == 0 || h == height-1) Debug.Log(count);
        return count;
    }

    void DrawMap()
    {

        for (int h = 0; h < height; ++h)
        {
            for (int w = 0; w < width; ++w)
            {
                if (map[h, w] == 1)
                    CreateTile(w - width/2, h - height/2, groundTiles[0]);
            }
        }
    }
    #endregion;

    void GenerateBase()
	{
		for (int i = 0; i < height; ++i)
			CreateBar (-i, -width / 2, width);
	}

	void CreateTile(int x, int y, GameObject tile)
	{
        GameObject t = Instantiate(tile, new Vector2(tileSize * x, tileSize * y), Quaternion.identity) as GameObject;
        t.name = tile.name + "[" + x + "," + y + "]";
	}

	void CreateBar(int height, int x, int length)
	{
		int end = x + length;
		for (int i = x; i < end; ++i)
			CreateTile(i, height, GetRandomObject(groundTiles, 0, 2));
	}

	GameObject GetRandomObject(GameObject[] objects, int from, int to)
	{
        return objects[UnityEngine.Random.Range(from, to)];
	}
}

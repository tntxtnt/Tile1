﻿using UnityEngine;
using System.Collections;

public class Damage
{
    public float damage;
    public Vector2 force;

    public Damage(float damage, Vector2 force)
    {
        this.damage = damage;
        this.force = force;
    }
}

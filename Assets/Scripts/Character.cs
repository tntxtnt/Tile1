﻿using UnityEngine;
using System.Collections;

public abstract class Character : MonoBehaviour
{
    public abstract void ApplyDamage(Damage d);
}

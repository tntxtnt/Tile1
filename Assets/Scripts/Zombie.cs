﻿using UnityEngine;
using System.Collections;

public class Zombie : Character
{
    public float hp = 10f;
    public float defense = 0f;

    public AudioClip hitSound;

    private Rigidbody2D rb2d;
    private Animator anim;
    private Moveable movement;

    private Transform playerTransform;

	void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        movement = GetComponent<Moveable>();

        playerTransform = GameObject.Find("Player").transform;
	}

    void FixedUpdate()
    {
        // Movement
        // get distance to player
        Vector2 distance = playerTransform.position - transform.position;
        // set proper move direction
        movement.h = Mathf.Sign(distance.x);

        // Jumping
        // only jump when player is about 2 blocks above and less than 2 blocks away
        if (distance.y > 0.44f && Mathf.Abs(distance.x) < .96f)
            movement.Jump();
    }

    public override void ApplyDamage(Damage d)
    {
        // calculate damage: attack - defense
        float dmg = d.damage - defense;
        // min damage is 1f
        if (dmg < 1f) dmg = 1f;
        // apply damage and force
        hp -= dmg;
        rb2d.AddForce(d.force);
        // should be some text damage appear on screen
        Debug.Log("Zombie Hp: " + hp);
        // ded?
        if (hp <= 0f)
            Destroy(gameObject);
    }
}

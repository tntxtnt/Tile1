﻿using UnityEngine;
using System.Collections;

public class Bow : Weapon
{
	public float timeBetweenShots = 0.5f;
    public float attack = 5f;
    public float fireForce = 30f;

    public AudioClip fireSound;

	[SerializeField]
	private Arrow[] arrows;
    private int arrowId = 0;

	//private bool fire = false;
	private float cooldown = -1f;

    private SpriteRenderer spriteRenderer;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;
    }

    new void FixedUpdate()
    {
        if (IsCooldown())
            cooldown -= Time.fixedDeltaTime;
        else
            spriteRenderer.enabled = false; // hide the bow
        base.FixedUpdate();
    }

    public override void SwitchAmmo()
    {
        arrowId = (arrowId + 1) % arrows.Length;
    }

    public override bool IsCooldown()
    {
        return cooldown > 0f;
    }

    protected override void Attack()
    {
        if (IsCooldown()) return;

        // show the bow
        spriteRenderer.enabled = true;

        // create arrow, add bow damage to it, and set its direction
        Arrow a = Instantiate(arrows[arrowId], transform.position, Quaternion.identity) as Arrow;
        a.AddBowDamage(attack);
        a.SetEnemyTag(enemyTag);
        a.ApplyForce(attackDirection.normalized * fireForce);

        // also set the bow sprite face to shooting direction
        float firingAngle = Vector2.Angle(Vector2.right, attackDirection);
        if (transform.parent.localScale.x < 0) // quick and dirty way to solve character facing left problem
            firingAngle = 180f - firingAngle;
        if (attackDirection.y < 0) firingAngle = -firingAngle;
        transform.rotation = Quaternion.Euler(0, 0, firingAngle);

        // make some noise!
        SoundManager.instance.PlaySingle(fireSound);

        // prevent too many shootings at the same time
        cooldown = timeBetweenShots;
    }
}
